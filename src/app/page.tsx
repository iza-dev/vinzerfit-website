import { RenderBuilderContent } from '@/components/builder';
import { builder } from '@builder.io/sdk';
import { Link } from '@chakra-ui/react';
import styles from './page.module.css';

builder.init(process.env.NEXT_PUBLIC_BUILDER_API_KEY!);

interface PageProps {
  params: {
    page: string[];
  };
}

export default async function Home(props: PageProps) {
  const builderModelName = 'home';

  const content = await builder
    .get(builderModelName, {
      userAttributes: {
        urlPath: '/' + (props?.params?.page?.join('/') || '')
      }
    })
    .toPromise();

  return (
    <>
      <main className={styles.main}>
        <h1>Welcome home!</h1>
        <p>
          Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ea neque, possimus ipsum
          voluptate ab culpa deleniti doloremque voluptatum? Ipsa praesentium ab saepe repellat.
          Ipsa maiores beatae repudiandae dolor libero ipsam suscipit rem fuga necessitatibus
          doloremque dignissimos reiciendis, dolores, numquam blanditiis assumenda id quod at ad,
          voluptatem eligendi quisquam facilis obcaecati.
        </p>
        <Link href="/programs">Programs</Link>
      </main>
      <RenderBuilderContent content={content} model={builderModelName} />
    </>
  );
}
